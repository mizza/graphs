<?php 


namespace app;

// класс, отвечающий за работу всего приложения, сделан по паттерну singleton

class App {
	public static $settings = array();
	public static $db;
	public static $controller;
	private static $_instance = null;
	private function __construct() {
	
	}
	protected function __clone() {
	
	}
	static public function getInstance($config) {
		if(is_null(self::$_instance))
		{

			self::$settings = $config;

			self::$_instance = new self();

			self::$db = new Db(self::$settings);

			self::$controller = \app\Controller::getInstance();

			self::$controller->doAction();
		}
		return self::$_instance;
	}
	
}

 ?>