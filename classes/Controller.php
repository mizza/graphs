<?php 

namespace app;


class Controller {

	private $action;
	private $args;
	private static $_instance = null;

	private function __construct() {
	
	}
	protected function __clone() {
	
	}
	static public function getInstance() {
		if(is_null(self::$_instance))
		{

			self::$_instance = new self();

		}
		return self::$_instance;
	}

	/**
	* Разбирает строку запроса. Если в параметре action указан действительный action, тогда запоминаем его, остальные параметры записываем в аргументы. Иначе все параметры записываем в аргументы
	* @param string $url
	* return void
	*/
	protected function parseUrl(){
		if (!empty($_GET['action']) && method_exists($this, $_GET['action'])){
			$this->action = $_GET['action'];
			unset($_GET['action']);
		}else{
			$this->action = null;
		}
		$this->args = $_GET;
		
	}

	public function doAction(){
		$this->parseUrl();
		if ($this->action){
			
			$this->{$this->action}();
		}
		else{
			$this->doDefault();
		}

	}

	private function getGraph(){
		if ($id = $this->args['id']){
			$graph = new \app\graphs\Graph($id);
			$aGraphs = \app\graphs\Graph::getList('name');
			$chosenGraph = $graph;
			$graphJSON = $graph->toJSON();
		}
		include('templates/main/index.php');
	}

	public function addNode(){
		$value = $this->args['value'];
		$x = $this->args['x'];
		$y = $this->args['y'];
		$graph = new \app\graphs\Graph($this->args['graph']);
		$graph->addNode($value, $x, $y);
	}

	// выводим первую страницу
	private function doDefault(){
		// список всех графов
		$aGraphs = \app\graphs\Graph::getList('name');

		include('templates/main/index.php');
	}

}

 ?>