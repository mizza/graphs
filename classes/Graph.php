<?php 

namespace app\graphs;


class Graph extends Table{

	/**
	* Возвращает название таблицы
	* return string
	*/
	protected function getTableName(){
		return "graphs";
	}

	/**
	* Простое добавление элемента 
	* @param string $value
	* return bool
	*/
	public function addNode($value, $x, $y){
		$iNode = Node::insertRecord(['value'=>$value, 'x'=>$x, 'y'=>$y, 'graph' => $this->id]);
		if ($iNode){
			return true;		
		}else{
			return false;
		}

	}

	/**
	* Добавление элемента после определенного узла
	* @param string $value
	* @param integer $node
	* return bool
	*/
	public function addNodeAfter($value, $node){
		
		$iNode = Node::insertRecord(['value'=>$value]);
		if ($iNode){
			// здесь же связываем их
			$iRel = $this->addRelation($node, $id);
			if ($iRel){
				return true;
			}else{
				return false;
			}
			
		}else{
			return false;
		}

	}

	/**
	* Добавление элемента перед определенным узлом
	* @param string $value
	* @param integer $node
	* return bool
	*/
	public function addNodeBefore($value, $node){
		
		$iNode = Node::insertRecord(['value'=>$value]);
		if ($iNode){
			$iRel = $this->addRelation($node, $id, 0);
			if ($iRel){
				return true;
			}else{
				return false;
			}
			
		}else{
			return false;
		}

	}

	/**
	* Удаление элемента
	* @param integer $id
	* return bool
	*/
	public function deleteNode($id){
		
		return Node::delete($id);

	}


	/**
	* Добавление связи между двумя узлами
	* @param integer $parent
	* @param integer $child
	* @param integer $number
	* return bool
	*/
	public function addRelation($parent, $child, $number = false){
		if ($number === false){
			$number = Node::getLastChildNumber($parent) + 10;
		}
		$attrs = [
			'parent' => $parent,
			'child' => $child,
			'graph' => $this->id,
			'number' => $number
		];
		$iRel = Relation::insertRecord($attrs);
		if ($iRel){
			return true;
		}else{
			return false;
		}
	}

	/**
	* Удаление связи между двумя узлами
	* @param integer $parent
	* @param integer $child
	* return bool
	*/
	public function deleteRelation($parent, $child){
		$attrs = [
			'parent' => $parent,
			'child' => $child,
			'graph' => $this->id
		];
		return Relation::deleteByAttributes($attrs);
	}

	/**
	* Обход графа в ширину
	* return Node[]
	*/
	// public bfs(){
	// 	// находим вершину, с которой начнем обход.
	// 	// Сначала ищем все вершины, у которых полустепень захода 0
	// 	// Если таких нет, то просто выбираем вершину с наименьшим id
	// 	$sql = "SELECT * FROM nodes WHERE graph = ".$this->id." "
	// 	$startNode = 
	// }

	/**
	* Преобразование вершин и ребер графа в JSON объект
	* return JSON
	*/
	public function toJSON(){
		$attrs = [
			'graph' => $this->id
		];
		$aNodes = Node::searchRecord($attrs);
		$aRels = Relation::searchRecord($attrs);

		$aNodesWithKeys = [];
		foreach ($aNodes as $key => $oNode) {
			$aNodesWithKeys[$oNode['id']] = $oNode;
		}
		$aRelsWithKeys = [];
		foreach ($aRels as $key => $oRel) {
			$aRelsWithKeys[$oRel['id']] = $oRel;
		}

		return json_encode([
			'id' => $this->id,
			'nodes' => $aNodesWithKeys,
			'rels' => $aRelsWithKeys
		]);

	}
	
}

 ?>