<?php 

namespace app\graphs;


class Node extends Table{

	/**
	* Возвращает название таблицы
	* return string
	*/
	protected function getTableName(){
		return "nodes";
	}

	/**
	* Возвращает всех своих потомков
	* return Node[]
	*/
	public function getChildren(){
		$attrs = [
			'graph' => $this->attributes['graph'],
			'parent' => $this->id
		];
		$aRel = Relation::searchRecord($attrs, "child", "number");

		$result = [];
		foreach ($aRel as $key => $iNode) {
			$result[] = new Node($iNode);
		}
		return $result;
	}

	/**
	* Возвращает всех своих предков
	* return Node[]
	*/
	public function getParents(){
		$attrs = [
			'graph' => $this->attributes['graph'],
			'child' => $this->id
		];
		$aRel = Relation::searchRecord($attrs, "parent");

		$result = [];
		foreach ($aRel as $key => $iNode) {
			$result[] = new Node($iNode);
		}
		return $result;
	}


	/**
	* Возвращает всех своих братьев
	* return Node[]
	*/
	public function getSiblings(){
		$attrs = [
			'graph' => $this->attributes['graph'],
			'parent' => $this->parent
		];
		$aRel = Relation::searchRecord($attrs, "*", "number");

		$result = [];
		foreach ($aRel as $key => $iNode) {
			$result[] = new Node($iNode);
		}
		return $result;
	}

	/**
	* Проверяет принадлежность предкам
	* @param array $parents
	* return boolean
	*/
	public function isChild($parents){
		$attrs = [
			'child' => $this->id,
			'graph' => $this->graph
		];		
		$aRel = Relation::searchRecord($attrs, "COUNT(*) AS count_rec");
		return !(empty($aRel) || ($aRel[0]['count_rec'] != count($parents)));


	}



	/**
	* Возвращает максимальную сортировку среди потомков (для добавления в конец)
	* return integer
	*/
	public function getLastChildNumber(){
		$attrs = [
			'graph' => $this->attributes['graph'],
			'parent' => $this->id
		];
		$aRel = Relation::searchRecord($attrs, "MAX(number) AS max_number");
		if (!empty($aRel)){
			return $aRel[0]['max_number'];
		}else{
			return 0;
		}

	}

	/**
	* Возвращает минимальную сортировку среди потомков (для добавления в начало) по id родителя
	* return integer
	*/
	public static function getFirstChildNumber($id){
		$attrs = [
			'graph' => $this->attributes['graph'],
			'parent' => $id
		];
		$aRel = Relation::searchRecord($attrs, "MIN(number) AS min_number");
		if (!empty($aRel)){
			return $aRel[0]['min_number'];
		}else{
			return 0;
		}

	}

	/**
	* Смена позиции потомка внутри заданного предка
	* return integer
	*/
	public function goToNumber($number)
	{
		$aSiblings = $this->getSiblings();
		if (count($aSiblings) < $number){
			$attr = [
				"number" => $aSiblings[count($aSiblings)]->number + 10
			];
		}else{
			$numberAfter = $aSiblings[$number-2]->number;
			$numberBefore = $aSiblings[$number-1]->number;
			$number = float(($numberBefore - $numberAfter)/2);
			$attr = [
				"number" => $aSiblings[count($aSiblings)]->number + 10
			];
		}
		return $this->update($attr);
	}


	
}

 ?>