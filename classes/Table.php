<?php 

namespace app\graphs;

use \app;

// абстрактный класс, определяющий структуру классов для работы с таблицами БД
abstract class Table{

	/**
	* Возвращает название таблицы
	* return string
	*/
	abstract protected function getTableName();

	public $attributes;
	

	function __construct($id) {
       $this->attributes = static::getRecord($id)[0];
   	}


   	// для доступа к полям записи в ООП - стиле
   	public function __get($name) 
    {
        
        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }else{
        	return null;
        }
    }

   	/**
	* Производит вставку в таблицу и возвращает id добавленной записи
	* @param array $attributes
	* return integer id
	*/
	public static function insertRecord($attributes){
		// оборачиваем все значения атрибутов
		array_walk($attributes, function($item){
			return "'$item'";
		});
		$keys = array_map(function($item){
			return "`$item`";
		}, array_keys($attributes));
		$keys = implode(',', $keys);
		$values = implode(',', $attributes);
		$sql = "INSERT INTO `".static::getTableName()."` ($keys) VALUES ($values)";
		// echo $sql;
		if ($res = \app\App::$db->doQuery($sql)){
			return mysql_insert_id();
		}else{
			return false;
		}
		
	}

	/**
	* Производит обновление записи в таблице
	* @param array $attributes
	* return boolean
	*/
	public function update($attributes){
		array_walk($attributes, function(&$a, $b) { $a = "`$b` = '$a'"; });
		$sUpdate = implode(' , ', $attributes);

		$sql = "UPDATE `".static::getTableName()."` SET $sUpdate WHERE id={$this->id}";
		return \app\App::$db->doQuery($sql);
	}

	/**
	* метод возвращает запись таблицы с ID = $id
	* @param integer $id
	* return array
	*/
	public static function getRecord($id){
		$sql = "SELECT * FROM `".static::getTableName()."` WHERE id = $id";
		return \app\App::$db->getQuery($sql);
	}

	/**
	* метод удаляет запись таблицы с ID = $id
	* @param integer $id
	* return Table
	*/
	public static function delete($id){
		$sql = "DELETE FROM `".static::getTableName()."` WHERE id = $id";
		return \app\App::$db->getQuery($sql);
	}

	/**
	* метод удаляет запись таблицы с заданными атрибутами
	* @param integer $id
	* return Table
	*/
	public static function deleteByAttributes($attributes){
		array_walk($attributes, function(&$a, $b) { $a = "`$b` = '$a'"; });
		$sWhere = implode(' AND ', $attributes);
		$sql = "DELETE FROM `".static::getTableName()."` WHERE $sWhere";
		return \app\App::$db->getQuery($sql);
	}

	/**
	* метод производит поиск по атрибутам
	* @param integer $id
	* return Table[]
	*/
	public static function searchRecord($attributes, $select = "*", $order = "id"){
		array_walk($attributes, function(&$a, $b) { $a = "`$b` = '$a'"; });
		$sWhere = implode(' AND ', $attributes);
		$sql = "SELECT $select FROM ".static::getTableName().($sWhere ? " WHERE $sWhere " : " ")."  ORDER BY $order";

		return \app\App::$db->getQuery($sql);
	}

	/**
	* метод возвращает массив всех элементов таблицы
	* @param string $fieldTitle
	* return Table[]
	*/
	public static function getList($fieldTitle){
		$aElements = static::searchRecord([], 'id, '.$fieldTitle);
		$result = [];
		foreach ($aElements as $key => $aElement) {
			$result[$aElement['id']] = $aElement[$fieldTitle];
		}
		return $result;

	}


}

 ?>