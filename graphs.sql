-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 09, 2015 at 11:55 PM
-- Server version: 5.5.41-log
-- PHP Version: 5.4.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `graphs`
--

-- --------------------------------------------------------

--
-- Table structure for table `graphs`
--

CREATE TABLE IF NOT EXISTS `graphs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'Название графа',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `graphs`
--

INSERT INTO `graphs` (`id`, `name`) VALUES
(1, 'Граф 1'),
(2, 'Граф 2');

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE IF NOT EXISTS `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(10) NOT NULL,
  `graph` int(11) NOT NULL,
  `x` int(11) NOT NULL COMMENT 'Координата по x',
  `y` int(11) NOT NULL COMMENT 'Координата по y',
  PRIMARY KEY (`id`),
  KEY `graph` (`graph`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `nodes`
--

INSERT INTO `nodes` (`id`, `value`, `graph`, `x`, `y`) VALUES
(1, '1', 1, 100, 100),
(2, '2', 1, 200, 100),
(9, '6', 1, 401, 142);

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE IF NOT EXISTS `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'код связи',
  `parent` int(11) DEFAULT NULL COMMENT 'родительская вершина',
  `child` int(11) DEFAULT NULL COMMENT 'дочерняя вершина',
  `number` int(11) NOT NULL COMMENT 'номер связи по порядку относительно родителя',
  `graph` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parent_2` (`parent`,`child`,`number`) COMMENT 'Уникальность связи',
  KEY `parent` (`parent`),
  KEY `child` (`child`),
  KEY `graph` (`graph`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица со связями' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `parent`, `child`, `number`, `graph`) VALUES
(1, 1, 2, 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `nodes`
--
ALTER TABLE `nodes`
  ADD CONSTRAINT `nodes_ibfk_1` FOREIGN KEY (`graph`) REFERENCES `graphs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `relations`
--
ALTER TABLE `relations`
  ADD CONSTRAINT `relations_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relations_ibfk_2` FOREIGN KEY (`child`) REFERENCES `nodes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_ibfk_3` FOREIGN KEY (`graph`) REFERENCES `graphs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
