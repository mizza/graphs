<!DOCTYPE html>
<html>
<head>
	<title>Работа с ориентированным графом</title>
	<meta charset="utf-8">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="/templates/main/js/paper-full.min.js"></script>
	<script type="text/javascript" src="/templates/main/js/graph.js"></script>
	<script type="text/javascript" src="/templates/main/js/main.js"></script>
</head>
<body>
	<div class="container">
		<h1>Работа с ориентированным графом</h1>
		<form class="form-horizontal">
		  <div class="form-group">
		    <label class="col-xs-2 control-label">Выбрать граф</label>
		    <div class="col-xs-10">
		    	<select class="form-control" id="graph-choose">
				  <?php foreach ($aGraphs as $key => $value) {?>
				  	<option value="<?=$key?>" <?if ($chosenGraph->id == $key){?>selected<?}?>><?=$value?></option>
				  <?} ?>
				</select>
		      
		    </div>
		  </div>
		</form>
		<canvas id="graph" resize></canvas>
	</div>

</body>
</html>
