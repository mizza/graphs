function Graph(config){

	var t = this;

	return {
		id : config.id,
		nodes: config.nodes,
		rels: config.rels,
		init: function(){
			paper.setup('canvas');
			t = this;
		},
		draw: function(){
			with (paper){
				
				for (var key in this.rels) {
					var xStart = parseInt(this.nodes[parseInt(this.rels[key].parent)].x);
					var yStart = parseInt(this.nodes[parseInt(this.rels[key].parent)].y);
					var xEnd = parseInt(this.nodes[parseInt(this.rels[key].child)].x);
					var yEnd = parseInt(this.nodes[parseInt(this.rels[key].child)].y);
					var myPath = new Path();
					myPath.strokeColor = 'black';
					myPath.add(new Point(xStart, yStart), new Point(xEnd, yEnd));
					myPath.onMouseDrag = function(event) {
				        this.position = event.point;
				    }
				}
				for (var key in this.nodes) {
					var x = parseInt(this.nodes[key].x) || 50;
					var y = parseInt(this.nodes[key].y) || 50;
					var point = new Path.Circle({
						center: [x, y], 
						radius: 30,
						strokeColor: 'black',
						fillColor: 'white'
					});
					var text = new PointText({
					    point: [x-8, y+8],
					    content: this.nodes[key].value,
					    fillColor: 'black',
					    fontFamily: 'Courier New',
					    fontWeight: 'bold',
					    fontSize: 25
					});
				}
				view.draw();

			};
		},
		addNodeModeOn: function(){
			$("#textinfo").text('Введите в поле название вершины и щелкните по полю, чтобы добавить ее');
			$("#hint").slideDown();
			$("#canvas").on('click', this.addNode);
			return false;
		},
		addNode: function(e){
			if (!$("#textfield").val()){
				var x = e.pageX - this.offsetLeft;
	    		var y = e.pageY - this.offsetTop;
	    		var point = new paper.Path.Circle({
					center: [x, y], 
					radius: 30,
					strokeColor: 'black',
					fillColor: 'white'
				});
				$.ajax({
					url: "?action=addNode&x=" + x + "&y=" + y + "&value=" + $("#textfield").val() + "&graph=" + t.id,
					success: function(){
						$("#canvas").off('click');
						$("#textinfo").text('');
						$("#hint").slideUp();
					}
				})
			}
			

		},



	}


}
